# Zabuyaki (Fork)

<img src="screenshot.webp" width=50% height=50%>

I'm not the original developer of this game, the original developer and graphic artist are below.

This is a fork of Zabuyaki (https://github.com/thomasgoldstein/zabuyaki).

I forked it to experiment and added / replaced a few things such as:

- Backgrounds (replaced with A.I. generated ones (stable diffusion))
- Added a pair of transparent light effects to the foreground paralax.
- Replaced a pair of assets on the intro and and stages.
- Modified the code to render backgrounds at higher resolution.

Since the developers have changed engine and now the game is built for megadrive (https://zabu.team/) no further enhancements should be expected on this early version of the game.

This is the original readme:

# Zabuyaki #
Zabuyaki is an unfinished beat 'em up made using the [LÖVE framework](https://love2d.org/).

Team:
* Programmer: [Don Miguel](https://github.com/D0NM)
* Game and graphics designer: [Stifu](https://github.com/thomasgoldstein)
* Assistant graphics designer: [Gege](https://github.com/atiplayer)
* Music composer and sound designer: [Juhani Junkala](https://soundcloud.com/juhanijunkala)

This game is being remade as [Zabu](https://zabu.team/) for the Sega Mega Drive / Genesis.