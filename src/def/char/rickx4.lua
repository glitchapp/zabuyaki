local spriteSheet = "res/img/char/rick.png"
local imageWidth,imageHeight = loadSpriteSheet(spriteSheet)

local function q(x,y,w,h)
    return love.graphics.newQuad(x, y, w, h, imageWidth, imageHeight)
end
local function f(n)
    return (n / 60) - ((n / 60) % 0.001) -- converts frames -> seconds. Usage: delay = f(4)
end
local airSfx = function(slf)
    slf:playSfx(sfx.air)
end
local stepFx = function(slf, cont)
    slf:showEffect("step")
    if stage.weather == "rain" then
        Weather.add("ripple", slf.x, slf.y, 0,slf.speed_x / 100, 0, 0, 1)
    end
end
local jumpAttackStraight1 = function(slf, cont) slf:checkAndAttack(
    { x = 17, z = 35, width = 30, height = 55, damage = 7 },
    cont
) end
local jumpAttackStraight2 = function(slf, cont) slf:checkAndAttack(
    { x = 17, z = 14, width = 30, damage = 10, type = "fell" },
    cont
) end
local jumpAttackForward = function(slf, cont) slf:checkAndAttack(
    { x = 30, z = 25, width = 25, height = 45, damage = 15, type = "fell" },
    cont
) end
local jumpAttackRun = function(slf, cont) slf:checkAndAttack(
    { x = 30, z = 25, width = 25, height = 45, damage = 17, type = "fell" },
    cont
) end
local jumpAttackLight = function(slf, cont) slf:checkAndAttack(
    { x = 15, z = 25, width = 22, damage = 9 },
    cont
) end
local comboSlide2 = function(slf)
    slf:initSlide(slf.comboSlideSpeed2_x)
end
local comboSlide3 = function(slf)
    slf:initSlide(slf.comboSlideSpeed3_x)
    slf.speed_z = slf.comboSlideSpeed3_z
    slf.z = slf:getRelativeZ() + 3
end
local comboSlide4 = function(slf)
    slf:initSlide(slf.comboSlideSpeed4_x)
end
local comboAttack1 = function(slf, cont)
    slf:checkAndAttack(
        { x = 28, z = 30, width = 26, damage = 7, sfx = "air" },
        cont
    )
end
local comboAttack2 = function(slf, cont)
    slf:checkAndAttack(
        { x = 28, z = 31, width = 27, damage = 8, sfx = "air" },
        cont
    )
end
local comboAttack2Forward = function(slf, cont)
    slf:checkAndAttack(
        { x = 21, z = 24, width = 31, damage = 8, repel_x = slf.comboSlideRepel2_x },
        cont
    )
end
local comboAttack3 = function(slf, cont)
    slf:checkAndAttack(
        { x = 28, z = 18, width = 27, damage = 10, sfx = "air" },
        cont
    )
end
local comboAttack3Up1 = function(slf, cont)
    slf:checkAndAttack(
        { x = 28, z = 30, width = 27, damage = 4, sfx = "air" },
        cont
    )
end
local comboAttack3Up2 = function(slf, cont)
    slf:checkAndAttack(
        { x = 18, z = 24, width = 27, damage = 9 },
        cont
    )
end
local comboAttack3Forward = function(slf, cont)
    slf:checkAndAttack(
        { x = 27, z = 21, width = 39, damage = 10, repel_x = slf.comboSlideRepel3_x },
        cont
    )
end
local comboAttack3Down = function(slf, cont)
    slf:checkAndAttack(
        { x = 29, z = 29, width = 27, damage = 15, type = "fell", sfx = "air" },
        cont
    )
end
local comboAttack4 = function(slf, cont)
    slf:checkAndAttack(
        { x = 34, z = 41, width = 39, damage = 15, type = "fell", sfx = "air" },
        cont
    )
end
local comboAttack4Up1 = function(slf, cont, attackId)
    slf:checkAndAttack(
        { x = 27, z = 40, width = 29, damage = 18, type = "fell" },
        cont, attackId
    )
end
local comboAttack4Up2 = function(slf, cont, attackId)
    slf:checkAndAttack(
        { x = 25, z = 50, width = 33, damage = 18, type = "fell" },
        cont, attackId
    )
end
local comboAttack4Forward = function(slf, cont)
    slf:checkAndAttack(
        { x = 35, z = 32, width = 39, damage = 15, type = "fell", repel_x = slf.comboSlideRepel4_x },
        cont
    )
end
local dashAttack1 = function(slf, cont) slf:checkAndAttack(
    { x = 20, z = 37, width = 55, damage = 8, repel_x = slf.dashAttackRepel_x },
    cont
) end
local dashAttack2 = function(slf, cont) slf:checkAndAttack(
    { x = 20, z = 37, width = 55, damage = 12, type = "fell", repel_x = slf.dashAttackRepel_x },
    cont
) end
local dashAttackSpeedUp = function(slf, cont)
    slf.speed_x = slf.dashAttackSpeed_x * 2
end
local dashAttackResetSpeed = function(slf, cont)
    slf.speed_x = slf.dashAttackSpeed_x
end
local chargeAttack1 = function(slf, cont, attackId)
    slf:checkAndAttack(
        { x = 34, z = 41, width = 39, damage = 15, type = "fell", twist = "weak" },
        cont, attackId
    )
end
local chargeAttack2 = function(slf, cont, attackId)
    slf:checkAndAttack(
        { x = 24, z = 41, width = 39, damage = 15, type = "fell", twist = "weak" },
        cont, attackId
    )
end
local chargeAttack3 = function(slf, cont, attackId)
    slf:checkAndAttack(
        { x = 14, z = 41, width = 39, damage = 15, type = "fell", twist = "weak" },
        cont, attackId
    )
end
local chargeDashAttack = function(slf, cont)
    slf:checkAndAttack(
        { x = 27, z = 21, width = 39, damage = 15, type = "fell" },
        cont
    )
end
local specialDefensiveShake = function(slf, cont)
    slf:playSfx(sfx.hitWeak1)
    mainCamera:onShake(0, 2, 0.03, 0.3)
end
local specialDefensive1 = function(slf, cont, attackId) slf:checkAndAttack(
    { x = 0, z = 32, width = 28, height = 32, depth = 18, damage = 25, type = "expel" },
    cont, attackId
) end
local specialDefensive2 = function(slf, cont, attackId) slf:checkAndAttack(
    { x = 10, z = 17, width = 50, height = 40, depth = 18, damage = 25, type = "expel" },
    cont, attackId
) end
local specialDefensive3 = function(slf, cont, attackId) slf:checkAndAttack(
    { x = 10, z = 42, width = 66, height = 90, depth = 18, damage = 25, type = "expel" },
    cont, attackId
) end
local specialDefensive4 = function(slf, cont, attackId) slf:checkAndAttack(
    { x = 5, z = 32, width = 40, height = 70, depth = 18, damage = 25, type = "expel" },
    cont, attackId
) end
local specialOffensive = function(slf, cont) slf:checkAndAttack(
    { x = 15, z = 37, width = 65, damage = 34, repel_x = slf.specialOffensiveRepel_x, type = "fell", twist = "strong" },
    cont
) end
local specialDash1 = function(slf, cont)
    slf:checkAndAttack(
        { x = 10, z = 18, width = 40, height = 35, damage = 6,
          onHit = function(slf)
              slf.isAttackConnected = true
              slf.customFriction = slf.dashAttackFriction * 2.5
          end
        }, cont)
end
local specialDashFollowUp = function(slf, cont)
    if slf.isAttackConnected then
        slf:setSprite("specialDash2")
        slf.speed_x = slf.dashAttackSpeed_x * 1.5
        slf.customFriction = slf.dashAttackFriction * 1.5
    end
end
local specialDashJumpStart = function(slf, cont)
    slf.speed_z = slf.jumpSpeed_z * 1.2
    slf.z = slf:getRelativeZ() + 0.01
    slf:showEffect("jumpStart")
end
local specialDash2a = function(slf, cont)
    slf:checkAndAttack(
        { x = 10, z = 18, width = 40, height = 35, damage = 6 },
        cont)
end
local specialDash2b = function(slf, cont, attackId)
    slf:checkAndAttack(
        { x = 10, z = 50, width = 40, height = 50, damage = 18, type = "fell", repel_x = slf.specialDashRepel_x },
        cont, attackId)
end
local grabFrontAttack = function(slf, cont)
    --default values: 10,0,20,12, "hit", slf.speed_x
    slf:checkAndAttack(
        { x = 18, z = 21, width = 26, damage = 9 },
        cont
    )
end
local grabFrontAttackLast = function(slf, cont)
    slf:checkAndAttack(
        { x = 18, z = 21, width = 26, damage = 11,
        type = "fell", repel_x = slf.shortThrowSpeed_x },
        cont
    )
end
local grabFrontAttackForwardHit = function(slf, cont)
    slf:checkAndAttack(
        { x = 20, z = 37, width = 55, damage = 10, type = "fell" },
        cont
    )
end
local grabFrontAttackForward = function(slf, cont)
    slf:initSlide(slf.comboSlideSpeed2_x * 1.25, -1, -1, slf.comboSlideSpeed2_x * 1.25)
end
local grabFrontAttackForward2 = function(slf, cont)
    slf:doThrow(slf.throwSpeed_x * 1.5, 0,
        150,
        slf.face, nil,
        slf.z + 1)
end
local grabFrontAttackBack = function(slf, cont)
    slf:doThrow(slf.throwSpeed_x * slf.throwSpeedHorizontalMutliplier, 0,
        slf.throwSpeed_z * slf.throwSpeedHorizontalMutliplier,
        slf.face, slf.face,
        slf.z + slf.throwStart_z)
end
local grabFrontAttackDown = function(slf, cont)
    slf:checkAndAttack(
        { x = 20, z = 30, width = 26, damage = 15,
        type = "fell", repel_x = slf.shortThrowSpeed_x },
        cont
    )
end
local grabBackAttack = function(slf, cont)
    local g = slf.grabContext
    if g and g.target then
        slf:checkAndAttack(
            { x = -38, z = 32, width = 40, height = 70, depth = 18, damage = slf.thrownFallDamage, type = "expel" },
            cont
        )
        local target = g.target
        slf:releaseGrabbed()
        target:applyDamage(slf.thrownFallDamage, "simple", slf)
        if target:canFall() then
            target:setState(target.fall)
            return
        end
        target:showHitMarks(slf.thrownFallDamage, 6, -12) --big hitmark
        target:setState(target.bounce)
    end
end
return {
    serializationVersion = 0.43, -- The version of this serialization process

    spriteSheet = spriteSheet, -- The path to the spritesheet
    spriteName = "rick", -- The name of the sprite

    delay = f(12), -- Default delay for all animations
    hurtBox = { width = 80, height = 200 }, -- Default hurtBox for all animations (4 times the original)

    animations = {
        icon = {
            { q = q(0, 56, 168, 68) }, -- Increase the values by 4 times
            delay = math.huge
        },
        stand = {
            { q = q(8, 12, 172, 252), ox = 80, oy = 248 }, -- Increase the values by 4 times
            { q = q(188, 8, 168, 256), ox = 80, oy = 252 }, -- Increase the values by 4 times
            { q = q(364, 12, 172, 252), ox = 80, oy = 248, delay = f(36) }, -- Increase the values by 4 times
            { q = q(544, 16, 172, 248), ox = 76, oy = 244, delay = f(56) }, -- Increase the values by 4 times
            loop = true,
            delay = f(48)
        },
        walk = {
            { q = q(8, 272, 140, 256), ox = 68, oy = 252 }, -- Increase the values by 4 times
            { q = q(148, 272, 140, 256), ox = 68, oy = 252 }, -- Increase the values by 4 times
            { q = q(288, 276, 140, 252), ox = 68, oy = 248, delay = f(60) }, -- Increase the values by 4 times
            { q = q(428, 272, 140, 256), ox = 68, oy = 252 }, -- Increase the values by 4 times
            { q = q(568, 272, 140, 256), ox = 68, oy = 252 }, -- Increase the values by 4 times
            { q = q(708, 276, 140, 252), ox = 68, oy = 248, delay = f(60) }, -- Increase the values by 4 times
            loop = true,
            delay = f(40)
        },
        run = {
            { q = q(8, 544, 168, 240), ox = 52, oy = 236 }, -- Increase the values by 4 times
            { q = q(188, 536, 192, 248), ox = 72, oy = 244, delay = f(28) }, -- Increase the values by 4 times
            { q = q(380, 536, 188, 248), ox = 68, oy = 244, func = stepFx }, -- Increase the values by 4 times
            { q = q(8, 800, 168, 240), ox = 48, oy = 236 }, -- Increase the values by 4 times
            { q = q(172, 792, 192, 244), ox = 72, oy = 244, delay = f(28) }, -- Increase the values by 4 times
            { q = q(364, 792, 188, 248), ox = 68, oy = 244, func = stepFx }, -- Increase the values by 4 times
            loop = true,
            delay = f(24)
        },
        squat = {
            { q = q(724, 28, 180, 236), ox = 80, oy = 232 }, -- Increase the values by 4 times
            delay = f(16)
        },
        land = {
            { q = q(724, 28, 180, 236), ox = 80, oy = 232 }, -- Increase the values by 4 times
            delay = f(16)
       },
sideStepUp = {
    { q = q(384, 3376, 176, 260), ox = 84, oy = 256 }, --side step up
    delay = math.huge
},
sideStepDown = {
    { q = q(568, 3376, 180, 244), ox = 88, oy = 244 }, --side step down
    delay = math.huge
},
jump = {
    { q = q(8, 7808, 156, 264), ox = 76, oy = 260, delay = f(9) }, --jump up
    { q = q(172, 7808, 176, 264), ox = 80, oy = 260, delay = f(5) }, --jump top
    { q = q(356, 7808, 180, 264), ox = 88, oy = 260 }, --jump down
    delay = math.huge
},
jumpAttackStraight = {
    { q = q(8, 3112, 152, 252), ox = 76, oy = 260, delay = f(8) }, --jump attack straight 1
    { q = q(160, 3112, 200, 256), ox = 76, oy = 260, func = jumpAttackStraight1, delay = f(3) }, --jump attack straight 2
    { q = q(368, 3112, 168, 244), ox = 76, oy = 260, funcCont = jumpAttackStraight2 }, --jump attack straight 3
    delay = math.huge
},
jumpAttackForward = {
    { q = q(8, 2856, 212, 244), ox = 92, oy = 260, delay = f(4) }, --jump attack forward 1
    { q = q(220, 2856, 300, 236), ox = 132, oy = 264, funcCont = jumpAttackForward }, --jump attack forward 2
    delay = math.huge
},
jumpAttackForwardEnd = {
    { q = q(8, 2856, 212, 244), ox = 92, oy = 260 }, --jump attack forward 1
    delay = math.huge
},
jumpAttackRun = {
    { q = q(8, 2856, 212, 244), ox = 92, oy = 260, delay = f(4) }, --jump attack forward 1
    { q = q(360, 1580, 308, 212), ox = 140, oy = 264, funcCont = jumpAttackRun }, --jump attack run
    delay = math.huge
},
jumpAttackRunEnd = {
    { q = q(8, 2856, 212, 244), ox = 92, oy = 260 }, --jump attack forward 1
    delay = math.huge
},
jumpAttackLight = {
    { q = q(8, 3376, 172, 264), ox = 84, oy = 260, delay = f(2) }, --jump attack light 1
    { q = q(176, 3376, 188, 252), ox = 92, oy = 264, funcCont = jumpAttackLight }, --jump attack light 2
    delay = math.huge
},
jumpAttackLightEnd = {
    { q = q(8, 3376, 172, 264), ox = 84, oy = 260 }, --jump attack light 1
    delay = math.huge
},
dropDown = {
    { q = q(8, 7808, 156, 264), ox = 76, oy = 260, delay = f(9) }, --jump up
    { q = q(172, 7808, 176, 264), ox = 80, oy = 260, delay = f(5) }, --jump top
    { q = q(356, 7808, 180, 264), ox = 88, oy = 260 }, --jump down
    delay = math.huge
},
respawn = {
    { q = q(356, 7808, 180, 264), ox = 88, oy = 260 }, --jump down
    { q = q(188, 1596, 168, 212), ox = 80, oy = 260 }, --stand up
    delay = f(8)
},


     pickUp = {
    { q = q(8,1580,172,244), ox = 80, oy = 240, delay = f(2) }, --pick up 1
    { q = q(188,1596,164,228), ox = 68, oy = 224, delay = f(12) }, --pick up 2
    { q = q(8,1580,172,244), ox = 80, oy = 240 }, --pick up 1
    delay = f(3)
},
combo1 = {
    { q = q(196,2076,240,252), ox = 76, oy = 248, func = comboAttack1, delay = f(4) }, --combo 1.1
    { q = q(8,2076,180,252), ox = 76, oy = 248 }, --combo 1.2
    delay = f(1)
},
combo2 = {
    { q = q(222,2076,156,252), ox = 64, oy = 248 }, --combo 2.1
    { q = q(312,2076,240,252), ox = 72, oy = 248, func = comboAttack2, delay = f(5) }, --combo 2.2
    { q = q(222,2076,156,252), ox = 64, oy = 248, delay = f(4) }, --combo 2.1
    delay = f(1)
},
        combo2Forward = {
    { q = q(536,2860,184,244), ox = 92, oy = 240, func = comboSlide2 }, --combo forward 2.1
    { q = q(724,2862,156,240), ox = 68, oy = 236 }, --combo forward 2.2
    { q = q(652,3106,216,240), ox = 68, oy = 236, funcCont = comboAttack2Forward, func = airSfx, delay = f(6) }, --combo forward 2.3
    { q = q(444,2076,156,252), ox = 64, oy = 248, delay = f(4) }, --combo 2.1
    delay = f(2)
},
combo3 = {
    { q = q(8,1712,176,252), ox = 84, oy = 248 }, --combo 3.1
    { q = q(196,1716,252,240), ox = 84, oy = 236, func = comboAttack3, delay = f(6) }, --combo 3.2
    { q = q(8,1712,176,252), ox = 84, oy = 248, delay = f(5) }, --combo 3.1
    delay = f(2)
},
combo3Up = {
    { q = q(396,12104,164,232), ox = 60, oy = 228 }, --special offensive 3 (shifted 7px left)
    { q = q(220,2174,244,244), ox = 76, func = comboAttack3Up1, oy = 240 }, --combo up 3.2
    { q = q(600,2174,212,248), ox = 80, func = comboAttack3Up2, oy = 244, delay = f(4) }, --combo up 3.3
    delay = f(6)
},
combo3Forward = {
    { q = q(704,1300,204,248), ox = 124, oy = 248, func = comboSlide3, delay = f(3) }, --combo 4.6
    { q = q(8,8084,204,244), ox = 116, oy = 252, delay = f(3) }, --charge dash attack 1
    { q = q(220,8080,288,236), ox = 100, oy = 252, funcCont = comboAttack3Forward, func = airSfx, delay = f(7) }, --charge dash attack 2
    { q = q(516,8080,232,256), ox = 92, oy = 252 }, --charge dash attack 3
    { q = q(540,7790,180,252), ox = 60, oy = 252 }, --charge dash attack 4
    { q = q(728,7790,172,256), ox = 68, oy = 252 }, --charge dash attack 5
    delay = f(2)
},
combo3Down = {
    { q = q(8,7028,160,256), ox = 84, oy = 252, delay = f(2) }, --dash attack 1
    { q = q(180,7032,156,248), ox = 84, oy = 244, delay = f(5) }, --dash attack 2
    { q = q(348,7032,216,248), ox = 112, oy = 244, delay = f(1) }, --dash attack 3
    { q = q(632,6216,192,248), ox = 92, oy = 244, delay = f(1) }, --combo down 3.1
    { q = q(624,6544,236,196), ox = 52, oy = 192, func = comboAttack3Down, delay = f(9) }, --combo down 3.2
    { q = q(324,7296,200,240), ox = 52, oy = 236 }, --combo down 3.3
    { q = q(216,8936,176,252), ox = 60, oy = 252 }, --special defensive 7
    comboEnd = true,
    delay = f(3)
},
combo4 = {
    { q = q(444,1712,172,248), ox = 64, oy = 248 }, --combo 4.1
    { q = q(620,1712,144,248), ox = 56, oy = 248, delay = f(3) }, --combo 4.2
    { q = q(8,8084,260,244), ox = 44, oy = 244, func = comboAttack4, delay = f(9) }, --combo 4.3
    { q = q(620,1712,144,248), ox = 56, oy = 248, delay = f(7) }, --combo 4.2
    delay = f(2)
},
combo4Up = {
    { q = q(8,4724,188,236), ox = 64, oy = 232, delay = f(8) }, --combo up 4.1
    { q = q(196,4724,184,248), ox = 60, oy = 244, delay = f(2) }, --combo up 4.2
    { q = q(392,4724,196,248), ox = 60, oy = 244, funcCont = comboAttack4Up1, attackId = 1, func = airSfx, delay = f(2) }, --combo up 4.3
    { q = q(588,4724,208,268), ox = 80, oy = 264, funcCont = comboAttack4Up2, attackId = 1 }, --combo up 4.4a
    { q = q(8,4996,212,260), ox = 80, oy = 256 }, --combo up 4.4b
    { q = q(220,4996,212,252), ox = 80, oy = 248 }, --combo up 4.4c
    { q = q(432,4996,184,252), ox = 76, oy = 248 }, --combo up 4.5
    delay = f(4)
},

combo4Forward = {
    { q = q(8,1064,196,248), ox = 136, oy = 244, func = comboSlide4 }, --combo forward 4.1
    { q = q(204,1064,204,248), ox = 92, oy = 244 }, --combo forward 4.2
    { q = q(408,1060,260,248), ox = 40, oy = 244, funcCont = comboAttack4Forward, func = airSfx, delay = f(9) }, --combo forward 4.3
    { q = q(676,1060,180,256), ox = 56, oy = 252, delay = f(7) }, --combo forward 4.4
    delay = f(4)
},
dashAttack = {
    { q = q(8,7028,160,256), ox = 84, oy = 252, delay = f(2) }, --dash attack 1
    { q = q(180,7032,156,248), ox = 84, oy = 244, delay = f(6) }, --dash attack 2
    { q = q(348,7032,216,248), ox = 112, oy = 244, delay = f(1) }, --dash attack 3
    { q = q(580,7028,160,244), ox = 60, oy = 240, func = dashAttackSpeedUp, delay = f(1) }, --dash attack 4
    { q = q(8,7140,272,252), ox = 80, oy = 252, func = dashAttack1, delay = f(5) }, --dash attack 5
    { q = q(288,7132,268,252), ox = 80, oy = 252, funcCont = dashAttack2, delay = f(5) }, --dash attack 5
    { q = q(564,7132,208,212), ox = 68, oy = 212, func = dashAttackResetSpeed, delay = f(4) }, --dash attack 6
    { q = q(8,7392,188,200), ox = 68, oy = 196, delay = f(8) }, --dash attack 7
    { q = q(204,7392,164,232), ox = 56, oy = 228 }, --dash attack 8
    { q = q(376,7384,160,248), ox = 60, oy = 244 }, --dash attack 9
    delay = f(3)
},
        chargeStand = {
    { q = q(2,1310,50,62), ox = 88, oy = 244, delay = f(16) }, --charge stand 1
    { q = q(54,1311,50,61), ox = 88, oy = 244 }, --charge stand 2
    { q = q(106,1311,49,61), ox = 88, oy = 244, delay = f(12) }, --charge stand 3
    { q = q(54,1311,50,61), ox = 88, oy = 244 }, --charge stand 2
    loop = true,
    delay = f(10)
},
chargeWalk = {
    { q = q(2,1374,52,62), ox = 88, oy = 244 }, --charge walk 1
    { q = q(56,1375,51,62), ox = 88, oy = 244 }, --charge walk 2
    { q = q(109,1374,51,63), ox = 88, oy = 244 }, --charge walk 3
    { q = q(2,1439,52,63), ox = 88, oy = 244 }, --charge walk 4
    { q = q(56,1440,52,62), ox = 88, oy = 244 }, --charge walk 5
    { q = q(110,1439,52,63), ox = 88, oy = 244 }, --charge walk 6
    loop = true,
    delay = f(12)
},
chargeAttack = {
    { q = q(113,584,43,62), ox = 64, oy = 248 }, --combo 4.1
    { q = q(158,584,36,62), ox = 56, oy = 248 }, --combo 4.2
    { q = q(2,650,65,61), ox = 44, oy = 244, funcCont = chargeAttack1, attackId = 1, func = airSfx, delay = f(5) }, --combo 4.3
    { q = q(69,650,50,61), ox = 48, oy = 244, funcCont = chargeAttack2, attackId = 1 }, --combo 4.4
    { q = q(121,649,53,62), ox = 80, oy = 248, funcCont = chargeAttack3, attackId = 1 }, --combo 4.5
    { q = q(176,650,51,62), ox = 124, oy = 248 }, --combo 4.6
    { q = q(138,779,46,63), ox = 88, oy = 252 }, --combo 4.7
    delay = f(3)
},
chargeDash = {
    { q = q(181,7,45,59), ox = 80, oy = 244, delay = f(4) }, --squat
    { q = q(164,1439,52,63), ox = 72, oy = 248 }, --charge dash
},
chargeDashAttack = {
    { q = q(176,650,51,62), ox = 124, oy = 248, delay = f(4) }, --combo 4.6
    { q = q(2,2021,51,61), ox = 116, oy = 252, delay = f(4) }, --charge dash attack 1
    { q = q(55,2021,72,59), ox = 100, oy = 252, funcCont = chargeDashAttack, delay = f(9) }, --charge dash attack 2
    { q = q(129,2021,58,64), ox = 96, oy = 252 }, --charge dash attack 3
    { q = q(136,1955,45,63), ox = 60, oy = 252 }, --charge dash attack 4
    { q = q(183,1954,43,64), ox = 68, oy = 252 }, --charge dash attack 5
    delay = f(3)
},
specialDefensive = {
    { q = q(2,1504,45,62), ox = 88, oy = 244 }, --special defensive 1
    { q = q(49,1505,49,61), ox = 96, oy = 244, delay = f(6) }, --special defensive 2
    { q = q(100,1505,45,61), ox = 68, oy = 244 }, --special defensive 3
    { q = q(147,1506,54,60), ox = 56, oy = 240, funcCont = specialDefensive1, attackId = 1 }, --special defensive 4
    { q = q(2,1568,58,57), ox = 56, oy = 236, funcCont = specialDefensive2, attackId = 1, func = specialDefensiveShake }, --special defensive 5a
    { q = q(62,1569,58,56), ox = 56, oy = 232, funcCont = specialDefensive2, attackId = 1 }, --special defensive 5b
    { q = q(122,1570,58,55), ox = 56, oy = 228, funcCont = specialDefensive3, attackId = 1 }, --special defensive 5c
    { q = q(122,1570,58,55), ox = 56, oy = 228, funcCont = specialDefensive3, attackId = 1 }, --special defensive 5c
    { q = q(122,1570,58,55), ox = 56, oy = 228, funcCont = specialDefensive4, attackId = 1 }, --special defensive 5c
    { q = q(122,1570,58,55), ox = 56, oy = 228, funcCont = specialDefensive4, attackId = 1 }, --special defensive 5c
    { q = q(122,1570,58,55), ox = 56, oy = 228 }, --special defensive 5c
    { q = q(2,1630,50,60), ox = 56, oy = 236 }, --special defensive 6
    { q = q(54,1627,44,63), ox = 60, oy = 252 }, --special defensive 7
    delay = f(3)
},
specialOffensive = {
    { q = q(2,2433,47,52), ox = 56, oy = 204 }, --special offensive 1
    { q = q(51,2430,46,55), ox = 28, oy = 216 }, --special offensive 2
    { q = q(99,2427,41,58), ox = 32, oy = 232 }, --special offensive 3
    { q = q(142,2425,59,60), ox = 56, oy = 236, funcCont = specialOffensive, delay = f(2) }, --special offensive 4
    { q = q(2,2487,44,61), ox = 48, oy = 244, delay = f(2) }, --special offensive 5
    { q = q(48,2489,41,59), ox = 56, oy = 252 }, --special offensive 6a
    { q = q(91,2490,41,58), ox = 56, oy = 252 }, --special offensive 6b
    { q = q(134,2490,41,58), ox = 56, oy = 252, delay = f(12) }, --special offensive 6c
    { q = q(138,779,46,63), ox = 68, oy = 252 }, --combo 4.7 (shifted 4px right)
    delay = f(3)
},
specialDash = {
    { q = q(2,2087,46,62), ox = 124, oy = 248 }, --special dash 1
    { q = q(50,2092,39,57), ox = 104, oy = 240 }, --special dash 2
    { q = q(91,2091,45,57), ox = 116, oy = 244 }, --special dash 3
    { q = q(138,2094,49,54), ox = 88, oy = 224, funcCont = specialDash1, func = dashAttackSpeedUp, delay = f(5) }, --special dash 4a
    { q = q(2,2153,49,54), ox = 88, oy = 224, funcCont = specialDash1, delay = f(5) }, --special dash 4b
    { q = q(53,2153,49,54), ox = 88, oy = 224, funcCont = specialDash1, delay = f(5) }, --special dash 4c
    { q = q(137,2302,44,56), ox = 64, oy = 224, func = specialDashFollowUp }, --special dash 13
    { q = q(183,2299,41,60), ox = 68, oy = 236 }, --special dash 14
    delay = f(4)
},
specialDash2 = {
    { q = q(104,2151,44,56), ox = 64, oy = 224 }, --special dash 5
    { q = q(150,2151,42,57), ox = 60, oy = 228, funcCont = specialDash2a }, --special dash 6
    { q = q(2,2228,44,59), ox = 56, oy = 232 }, --special dash 7
    { q = q(48,2213,46,74), ox = 88, oy = 272, funcCont = specialDash2b, attackId = 1, func = specialDashJumpStart, delay = f(6) }, --special dash 8
    { q = q(48,2213,46,74), ox = 88, oy = 272, funcCont = specialDash2b, attackId = 1, delay = f(5) }, --special dash 8
    { q = q(48,2213,46,74), ox = 88, oy = 272, funcCont = specialDash2b, attackId = 1, delay = f(5) }, --special dash 8
    { q = q(96,2214,45,73), ox = 104, oy = 272, delay = f(6) }, --special dash 9
    { q = q(2,2289,38,70), ox = 84, oy = 260, delay = f(6) }, --special dash 10
    { q = q(42,2292,43,66), ox = 92, oy = 252, delay = f(6) }, --special dash 11
    { q = q(87,2296,48,63), ox = 100, oy = 248, delay = f(6) }, --special dash 12
    { q = q(137,2302,44,56), ox = 64, oy = 224, func = function(slf) slf:showEffect("jumpLanding") end }, --special dash 13
    { q = q(183,2299,41,60), ox = 68, oy = 236 }, --special dash 14
    delay = f(4)
},
carry = {
    { q = q(2,1181,47,59), ox = 64, oy = 232 }, --combo up 4.1
    { q = q(51,1178,46,62), ox = 60, oy = 236 }, --combo up 4.2
    { q = q(99,1178,49,62), ox = 60, oy = 236 }, --combo up 4.3
    { q = q(150,1173,52,67), ox = 80, oy = 252 }, --combo up 4.4a
    { q = q(2,1242,53,65), ox = 80, oy = 252 }, --combo up 4.4b
    { q = q(57,1244,53,63), ox = 80, oy = 252 }, --combo up 4.4c
    { q = q(112,1244,46,63), ox = 76, oy = 252 }, --combo up 4.5
    delay = f(2),
    moves = {
        { ox = 140, oz = 48, tAnimation = "thrown10h" },
        { ox = 160, oz = 56 },
        { ox = 160, oz = 64 },
        { ox = 120, oz = 76, tAnimation = "thrown12h" },
        { ox = 100, oz = 84 },
        { ox = 40, oz = 92, tAnimation = "stand" },
        { ox = 0, oz = 96 }
    }
},

       grab = {
    { q = q(8,3916,176,252), ox = 72, oy = 248 }, --grab
    delay = math.huge
},
grabSwap = {
    { q = q(536,7548,168,248), ox = 64, oy = 248 }, --grab swap 1.1
    { q = q(712,7548,140,248), ox = 68, oy = 248 }, --grab swap 1.2
    delay = math.huge
},
grabFrontAttack1 = {
    { q = q(192,3916,216,252), ox = 124, oy = 248 }, --grab attack 1
    { q = q(416,3920,168,248), ox = 76, oy = 248, delay = f(1) }, --grab attack 2
    { q = q(592,3936,180,232), ox = 64, oy = 228, func = grabFrontAttack, delay = f(10) }, --grab attack 3
    { q = q(728,1796,156,240), ox = 68, oy = 236 }, --combo forward 2.2
    delay = f(2)
},
grabFrontAttack2 = {
    { q = q(192,3916,216,252), ox = 124, oy = 248 }, --grab attack 1
    { q = q(416,3920,168,248), ox = 76, oy = 248, delay = f(1) }, --grab attack 2
    { q = q(592,3936,180,232), ox = 64, oy = 228, func = grabFrontAttack, delay = f(10) }, --grab attack 3
    { q = q(728,1796,156,240), ox = 68, oy = 236 }, --combo forward 2.2
    delay = f(2)
},
grabFrontAttack3 = {
    { q = q(192,3916,216,252), ox = 124, oy = 248 }, --grab attack 1
    { q = q(416,3920,168,248), ox = 76, oy = 248, delay = f(1) }, --grab attack 2
    { q = q(592,3936,180,232), ox = 64, oy = 228, func = grabFrontAttackLast, delay = f(10) }, --grab attack 3
    { q = q(728,1796,156,240), ox = 68, oy = 236 }, --combo forward 2.2
    delay = f(2)
},
grabFrontAttackForward = {
    { q = q(8,9444,224,248), ox = 132, oy = 244 }, --throw forward 1
    { q = q(240,9448,168,244), ox = 68, oy = 244 }, --throw forward 2
    { q = q(416,9444,252,248), ox = 80, oy = 248, func = grabFrontAttackForward, funcCont = grabFrontAttackForwardHit, delay = f(18) }, --throw forward 3
    { q = q(676,9472,216,212), ox = 68, oy = 208, func = grabFrontAttackForward2, delay = f(4) }, --throw forward 4
    { q = q(8,7596,188,200), ox = 68, oy = 196, delay = f(8) }, --dash attack 7
    { q = q(204,7576,164,232), ox = 56, oy = 232 }, --dash attack 8
    { q = q(376,7544,152,248), ox = 60, oy = 244 }, --dash attack 9
    delay = f(3),
    isThrow = true,
    moves = {
        { oz = 0, ox = 104 },
        { oz = 0, ox = 120 },
        { oz = 4, ox = 160, z = 0.001, cont = true, tAnimation = "fall" }, -- z > 0 for no friction, cont to apply offset on every draw
    }
},
grabFrontAttackBack = {
    { q = q(8,4436,168,252), ox = 92, oy = 248 }, --throw back 1
    { q = q(184,4452,196,216), ox = 36, oy = 216, delay = f(3) }, --throw back 2
    { q = q(392,4476,236,192), ox = 80, oy = 192, func = grabFrontAttackBack }, --throw back 3
    { q = q(636,4452,204,216), ox = 36, oy = 216, delay = f(4) }, --throw back 4
    { q = q(844,4712,156,248), ox = 40, oy = 248, delay = f(2) }, --throw back 5
    delay = f(12),
    isThrow = true,
    moves = {
        { ox = 20, oz = 96, z = 0 },
        { ox = 40, oz = 80 }
    }
},
grabFrontAttackDown = {
    { q = q(8,3988,224,252), ox = 120, oy = 248, delay = f(15) }, --grab attack end 1
    { q = q(240,3996,216,236), ox = 68, oy = 232, func = grabFrontAttackDown, delay = f(3) }, --grab attack end 2
    { q = q(468,3988,208,240), ox = 68, oy = 236, delay = f(12) }, --grab attack end 3
    { q = q(684,3996,176,252), ox = 68, oy = 248 }, --grab attack end 4
    delay = f(6)
},
grabBackAttack = {
    { q = q(8,6776,164,244), ox = 48, oy = 240, delay = f(8) }, --back throw 1
    { q = q(184,6772,180,252), ox = 60, oy = 248, delay = f(6) }, --back throw 2
    { q = q(376,6788,244,200), ox = 156, oy = 196, delay = f(5) }, --back throw 3
    { q = q(624,6772,240,216), ox = 192, oy = 212, delay = f(3) }, --back throw 4
    { q = q(408,6544,252,152), ox = 204, oy = 136, func = grabBackAttack, delay = f(18) }, --back throw 5
    { q = q(576,1860,220,208), ox = 120, oy = 204 }, --get up
    { q = q(188,1596,164,228), ox = 68, oy = 224 }, --pick up 2
    { q = q(8,1580,172,244), ox = 80, oy = 240, delay = f(3) }, --pick up 1
    delay = f(9),
    isThrow = true,
    moves = {
        { oz = 4, tAnimation = "thrown12h" },
        { oz = 16, tAnimation = "thrown12h" },
        { oz = 48, ox = 28, tAnimation = "thrown10h" },
        { oz = 112, ox = -68, tAnimation = "thrown8h" },
        { oz = -20, ox = -192, tAnimation = "fallOnHead" },
    }
},
hurtHighWeak = {
    { q = q(192,1324,188,248), ox = 104, oy = 244, delay = f(12) }, --hurt high 1
    { q = q(8,1320,176,252), ox = 88, oy = 248 }, --hurt high 2
    delay = f(2)
},
hurtHighMedium = {
    { q = q(192,1324,188,248), ox = 104, oy = 244, delay = f(18) }, --hurt high 1
    { q = q(8,1320,176,252), ox = 88, oy = 248 }, --hurt high 2
    delay = f(3)
},
hurtHighStrong = {
    { q = q(192,1324,188,248), ox = 104, oy = 244, delay = f(24) }, --hurt high 1
    { q = q(8,1320,176,252), ox = 88, oy = 248 }, --hurt high 2
    delay = f(4)
},
hurtLowWeak = {
    { q = q(576,1324,176,248), ox = 72, oy = 244, delay = f(12) }, --hurt low 1
    { q = q(408,1320,180,252), ox = 80, oy = 248 }, --hurt low 2
    delay = f(2)
},
hurtLowMedium = {
    { q = q(576,1324,176,248), ox = 72, oy = 244, delay = f(18) }, --hurt low 1
    { q = q(408,1320,180,252), ox = 80, oy = 248 }, --hurt low 2
    delay = f(3)
},

        hurtLowStrong = {
    { q = q(576,1324,176,248), ox = 72, oy = 244, delay = f(24) }, --hurt low 1
    { q = q(408,1320,180,252), ox = 80, oy = 248 }, --hurt low 2
    delay = f(4)
},
fall = {
    { q = q(145,136,56,60), ox = 124, oy = 118, delay = f(20) }, --fall 1
    { q = q(144,211,63,49), ox = 152, oy = 96, delay = f(8) }, --fall 2
    { q = q(2,484,69,33), ox = 164, oy = 16 }, --fall 3
    delay = math.huge
},
fallTwistWeak = {
    { q = q(69,2652,60,58), ox = 152, oy = 158 }, --fall twist 2
    { q = q(131,2652,58,62), ox = 150, oy = 158 }, --fall twist 3
    { q = q(191,2652,56,57), ox = 160, oy = 158 }, --fall twist 4
    { q = q(2,2659,65,54), ox = 160, oy = 110, delay = math.huge }, --fall twist 1
    delay = f(8)
},
fallTwistStrong = {
    { q = q(2,2659,65,54), ox = 160, oy = 110 }, --fall twist 1
    { q = q(69,2652,60,58), ox = 152, oy = 158 }, --fall twist 2
    { q = q(131,2652,58,62), ox = 150, oy = 158 }, --fall twist 3
    { q = q(191,2652,56,57), ox = 160, oy = 158 }, --fall twist 4
    loop = true,
    delay = f(5)
},
fallBounce = {
    { q = q(73,485,69,32), ox = 150, oy = 94, delay = f(4) }, --fallen
    { q = q(2,484,69,33), ox = 164, oy = 16 }, --fall 3
    delay = math.huge
},
fallenDead = {
    { q = q(73,485,69,32), ox = 150, oy = 94 }, --fallen
    delay = math.huge
},
fallOnHead = {
    { q = q(2,484,69,33), ox = 116, oy = 12, rotate = -1.57 }, --fall 3 (rotated -90°)
    { q = q(144,211,63,49), ox = 116, oy = 18, rotate = -1.57, delay = f(4) }, --fall 2 (rotated -90°)
    { q = q(145,136,56,60), ox = 116, oy = 20, rotate = -1.57 }, --fall 1 (rotated -90°)
    { q = q(2,484,69,33), ox = 164, oy = 16, delay = f(4) }, --fall 3
    delay = f(5)
},
getUp = {
    { q = q(73,485,69,32), ox = 150, oy = 94, delay = f(24) }, --fallen
    { q = q(144,465,55,52), ox = 124, oy = 102 }, --get up
    { q = q(47,399,41,57), ox = 92, oy = 100 }, --pick up 2
    { q = q(2,395,43,61), ox = 100, oy = 96 }, --pick up 1
    delay = f(9)
},
grabbedFront = {
    { q = q(2,330,44,63), ox = 22, oy = 62 }, --hurt high 2
    { q = q(48,331,47,62), ox = 26, oy = 61 }, --hurt high 1
    delay = f(2)
},
grabbedBack = {
    { q = q(97,330,45,63), ox = 20, oy = 62 }, --hurt low 2
    { q = q(144,331,44,62), ox = 18, oy = 61 }, --hurt low 1
    delay = f(2)
},
thrown = {
    --rx = ox / 2, ry = -oy / 2 for this rotation
    { q = q(145,136,56,60), ox = 124, oy = 118, rotate = -1.57, rx = 92, ry = -89, delay = f(24) }, --fall 1 (rotated -90°)
    { q = q(2,484,69,33), ox = 164, oy = 16 }, --fall 3
    delay = math.huge
},
thrown12h = {
    { q = q(144,331,44,62), ox = 18, oy = 61 }, --hurt low 1
    delay = math.huge
},
thrown10h = {
    { q = q(145,137,56,59), ox = 124, oy = 116 }, --fall 1
    delay = math.huge
},
thrown8h = {
    { q = q(145,136,56,60), ox = 124, oy = 118, rotate = -1.57, rx = 124, ry = -89 }, --fall 1 (rotated -90°)
    delay = math.huge
},
thrown6h = {
    { q = q(144,331,44,62), ox = 18, oy = 61, flipH = -1, flipV = -1 }, --hurt low 1 (flipped horizontally and vertically)
    delay = math.huge
},

    }
}
