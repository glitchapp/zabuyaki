-- Display resolution, inner canvas, zooming settings
return { -- normal resolution
    rawCanvas = {
        resolution = { width = 640, height = 480 },
        scale = 0.5,
    },
    gameWindowCanvas = {
        resolution = { width = 1920, height = 1080 },
        scale = 1,
    },
}
